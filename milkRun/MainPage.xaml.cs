﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using milkRun.recordOhjects;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;



namespace milkRun
{
    public partial class MainPage : PhoneApplicationPage
    {

        public ObservableCollection<UserBasket> userBaskets { get; set; }
        private applicationSettings appSettings = new applicationSettings();
        private IsolatedStorageSettings userSettings = IsolatedStorageSettings.ApplicationSettings;
        // Constructor
        public MainPage()
        {
            InitializeComponent();



            //set data context bindings
            welcomeText.DataContext = appSettings.getPhrase("welcomeText");
            bannerText.DataContext = appSettings.getPhrase("userName");

            _basket.IsOpen = false;

            try
            {
                userBaskets = (App.Current as App).appUser.getUserBasketsToOC();
            }
            catch (Exception ex)
            {
                int tId = (App.Current as App).appUser.addUpdateUserBaskets(new UserBasket(true));
                userBaskets = new ObservableCollection<UserBasket>();
                userBaskets.Add((App.Current as App).appUser.getUserBasket(tId));
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
            }

            
            userBasketList.DataContext = userBaskets;
        }


       
        /// <summary>
        /// Select an existing basket
        /// </summary>
        private void selectBasket(object sender, RoutedEventArgs e)
        {
            Button clickedButton = sender as Button;
            UserBasket selectedBasket = clickedButton.DataContext as UserBasket;

            NavigationService.Navigate(new Uri("/editBasket.xaml?basketID=" + selectedBasket.basketID, UriKind.RelativeOrAbsolute));

        }

        /// <summary>
        /// Create a new basket
        /// </summary>
        private void newBasketButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/editBasket.xaml", UriKind.RelativeOrAbsolute));
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/viewShoppedBaskets.xaml", UriKind.RelativeOrAbsolute));
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/mapShoppedBasket.xaml", UriKind.RelativeOrAbsolute));
        }
        private void button4_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/selectShoppedBasket.xaml", UriKind.RelativeOrAbsolute));
        }

    }
}