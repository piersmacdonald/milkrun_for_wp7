﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using milkRun.recordOhjects;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;

namespace milkRun
{
    public partial class viewShoppedBaskets : PhoneApplicationPage
    {

        private SearchedUserBasket shoppedBasket { get; set; }
        public ObservableCollection<BasketSearchResults> searchResults { get; set; } 
        
        public viewShoppedBaskets()
        {
            InitializeComponent();
            test_CreateSearchedBasket();

            searchResults = ListToCollection(shoppedBasket.searchResults);
            DataContext = this;
    
        }

        private void selectBasket(object sender, RoutedEventArgs e)
        {
            
            Button clickedButton = sender as Button;
            BasketSearchResults selectedBasket = clickedButton.DataContext as BasketSearchResults;  

            NavigationService.Navigate(new Uri("/mapShoppedBasket.xaml?basketPrice=" + selectedBasket.basketPrice , UriKind.RelativeOrAbsolute));
    
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string input;
            UserBasket toBeSearched;

            if (NavigationContext.QueryString.TryGetValue("basketID", out input))
            {

                int bID = Int32.Parse(input);
                //get userBasket by ID from the appuser
                try
                {
                    toBeSearched = (App.Current as App).appUser.getUserBasket(bID);
                    test_CreateSearchedBasket(toBeSearched);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("couldn't create test basket");
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                    //shit went down. add more robust exp handling later
                }

            }
            else
            {
                toBeSearched = new UserBasket();
                test_CreateSearchedBasket(toBeSearched);
            }
            //TODO: web service call to shop this basket
            

        }

        private void test_CreateSearchedBasket(UserBasket toBeSearched)
        {
            shoppedBasket = new SearchedUserBasket(toBeSearched);
        }

        private void test_CreateSearchedBasket()
        {
            shoppedBasket = new SearchedUserBasket(true);
        }

        private ObservableCollection<BasketSearchResults> ListToCollection(List<BasketSearchResults> list)
        {
            ObservableCollection<BasketSearchResults> newCollection = new ObservableCollection<BasketSearchResults>();
            foreach (BasketSearchResults i in list)
            {
                newCollection.Add(i);
            }

            return newCollection;
        }
    }
}