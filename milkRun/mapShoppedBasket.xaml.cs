﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using System.Device.Location;
using System.Threading;


namespace milkRun
{
    public partial class mapShoppedBaskets : PhoneApplicationPage
    {
        GeoCoordinateWatcher watcher;
        bool trackingOn = false;
        List<Pushpin> storePins = new List<Pushpin>();
        Pushpin myLocationPin = new Pushpin();

        public mapShoppedBaskets()
        {
            InitializeComponent();

            mapSelectedStores(whereAmIFakeStub(), mapServiceFakeStub());
        }


        private void mapSelectedStores(GeoCoordinate myLoc, basketMappingResults mapServiceResults) 
        {
            
      
            myLocationPin.Location = myLoc;

            foreach(GeoCoordinate i in mapServiceResults.storeLocations)
            {
                Pushpin newPin = new Pushpin();
                newPin.Location = i;
                storePins.Add(newPin);
            }


            //set map parameters for ideal viewing
            basketMap.Center = myLoc;
            basketMap.ZoomLevel = 14;
            basketMap.Children.Add(myLocationPin);

            foreach (Pushpin i in storePins)
            {
                basketMap.Children.Add(i);
            }
            
        }

        //junk for throw away
        private basketMappingResults mapServiceFakeStub()
        {

            GeoCoordinate storeTest = new GeoCoordinate(49.24845047195197, -123.12628984451294);
            basketMappingResults returnMe = new basketMappingResults();
            List<GeoCoordinate> storesTest = new List<GeoCoordinate>();
            storesTest.Add(storeTest);
            returnMe.storeLocations = storesTest;
            return returnMe;
        }

        //junk method
        private GeoCoordinate whereAmIFakeStub()
        {
            //my place on cambie
            return new GeoCoordinate(49.25097869677314, -123.11487436294556);
        }

    }
}