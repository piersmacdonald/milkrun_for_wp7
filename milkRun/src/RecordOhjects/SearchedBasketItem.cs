﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using milkRun.Utils;

namespace milkRun.recordOhjects
{
    public class SearchedBasketItem : BasketItem 
    {
        public string listPrice { get; set; }
        public Store store { get; set; }

        /// <summary>
        /// Testing constructor
        /// </summary>
        public SearchedBasketItem(string newItemDesc, string newItemBrand)
        {
            Random random = new Random();
            basketItemID = random.Next(1000);

            itemDescription = newItemDesc;
            brandName = newItemBrand;
        }

        //Garbage method
        public SearchedBasketItem(Decimal price)
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 10);

            listPrice = String.Format("{0:C}", price); 
            itemDescription = RandomString(randomNumber);


        }

        public SearchedBasketItem(Decimal price, string desc)
        {
            listPrice = String.Format("{0:C}", price);
            itemDescription = desc;
        }

        public SearchedBasketItem(BasketItem i)
        {
            Random random = new Random();
            this.listPrice = String.Format("{0:C}", PriceUtils.NextDecimal(random));
            this.itemDescription = i.itemDescription;
        }

        private readonly Random _rng = new Random();
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private string RandomString(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
            }
            return new string(buffer);
        }
    }
}
