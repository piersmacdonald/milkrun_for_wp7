﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;


namespace milkRun.recordOhjects
{
    public class AppUser
    {
        public int userID { get; private set; }
        public string userName { get; private set; }
        private Dictionary<int, UserBasket> baskets { get; set; }
      

        public AppUser(int id, string name)
        {
            userID = id;
            userName = name;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userLocalSavedBaskets"))
            {
                baskets = IsolatedStorageSettings.ApplicationSettings["userLocalSavedBaskets"] as Dictionary<int, UserBasket>;
            }
            else
            {
                baskets = new Dictionary<int, UserBasket>();
            }
        }

        public UserBasket getUserBasket(int basketID)
        {
            if (baskets.ContainsKey(basketID))
            {
                return baskets[basketID];
            }
            else
            {
                return null;
            }           
        }

        public List<UserBasket> getUserBasketsToList()
        {
            if (baskets.Count > 0)
            {
                List<UserBasket> bList = new List<UserBasket>();
                foreach (UserBasket u in baskets.Values)
                {
                    bList.Add(u);
                }

                return bList;
            }
            else
            {
                throw new NoBasketsException();
            }
        }

        public ObservableCollection<UserBasket> getUserBasketsToOC()
        {
            if (baskets.Count > 0)
            {
                ObservableCollection<UserBasket> bList = new ObservableCollection<UserBasket>();
                foreach (UserBasket u in baskets.Values)
                {
                    bList.Add(u);
                }
                return bList;
            }
            else
            {
                throw new NoBasketsException();
            }

            
        }

        public void saveUserBaskets()
        {
            IsolatedStorageSettings userLocalSettings = IsolatedStorageSettings.ApplicationSettings;

            if (!userLocalSettings.Contains("userLocalSavedBaskets"))
            {
                userLocalSettings.Add("userLocalSavedBaskets", baskets);
            }
            else
            {
                userLocalSettings["userLocalSavedBaskets"] = baskets;
            }
            userLocalSettings.Save();
        }

        public int addUpdateUserBaskets(UserBasket basket)
        {
            if (baskets.ContainsKey(basket.basketID))
            {
                baskets[basket.basketID] = basket;
            }
            else
            {
                if (baskets.Count > 0)
                {
                    //get a list of keys
                    List<int> keys = new List<int>(baskets.Keys);

                    //sort the list
                    keys.Sort();

                    //add the new basket with the highest basket number + 1
                    //later this will all be processed in the cloud
                    basket.basketID = keys[keys.Count - 1] + 1;
                    baskets.Add(basket.basketID, basket);
                }
                else
                {
                    basket.basketID = 1;
                    baskets.Add(1, basket);
                }
            }

            return basket.basketID;
        }

    }
}
