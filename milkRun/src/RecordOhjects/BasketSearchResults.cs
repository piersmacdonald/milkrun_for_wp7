﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using milkRun.Utils;

namespace milkRun.recordOhjects
{
    public class BasketSearchResults
    {

        public Store store { get; set; }
        public string basketPrice { get; set; }
        public List<SearchedBasketItem> searchedItemList { get; set; }

        int createdByID { get; set; }
        int lastModifedID { get; set; }
        DateTime createdTS { get; set; }
        DateTime lastModifiedTS { get; set; }
        DateTime effStartTS { get; set; }
        DateTime effStopTS { get; set; }

        public BasketSearchResults(bool test) 
        {
            searchedItemList = new List<SearchedBasketItem>();
            store = new Store();
            Random random = new Random();
            basketPrice = String.Format("{0:C}", PriceUtils.NextDecimal(random));
           
           


            switch(random.Next(2)){
                case 0:
                    searchedItemList.Add(new SearchedBasketItem(3.99m,"Rootbeer"));
                    searchedItemList.Add(new SearchedBasketItem(2.75m, "Soda Crackers"));
                    searchedItemList.Add(new SearchedBasketItem(4.59m, "Crest Toothpaste"));
                    break;

                case 1:
                    searchedItemList.Add(new SearchedBasketItem(5.59m, "Chocolate Chips"));
                    searchedItemList.Add(new SearchedBasketItem(9.75m, "Frozen Pizza"));
                    searchedItemList.Add(new SearchedBasketItem(6.19m, "Pickles"));
                    break;
                case 2:
                    searchedItemList.Add(new SearchedBasketItem(5.99m, "Tetley Tea"));
                    searchedItemList.Add(new SearchedBasketItem(2.89m, "Green Giant peas"));
                    searchedItemList.Add(new SearchedBasketItem(7.65m, "Dinner Rolls (x15)"));
                    break;
            }
        }

        public BasketSearchResults(UserBasket inputBasket)
        {
            Random random = new Random();
            searchedItemList = new List<SearchedBasketItem>();

            foreach (BasketItem i in inputBasket.basketItems)
            {
                searchedItemList.Add(new SearchedBasketItem(i));
            }
            basketPrice = String.Format("{0:C}", PriceUtils.NextDecimal(random));
            store = new Store();
            
        }



        private readonly Random _rng = new Random();
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private string RandomString(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
            }
            return new string(buffer);
        }

    }
}
