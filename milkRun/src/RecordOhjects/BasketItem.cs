﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace milkRun.recordOhjects
{
    public class BasketItem
    {
        public int basketItemID { get; set; }
        public string brandName { get; set; }
        public string itemDescription { get; set; }
     

        public BasketItem() { }

        public BasketItem(int newItemID)
        {
            // TODO: Complete member initialization
            this.basketItemID = newItemID;
        }



        //method for testing. won't be used
        public BasketItem(string newItemDesc, string newItemBrand)
        {
            Random random = new Random();
            basketItemID = random.Next(1000);

            itemDescription = newItemDesc;
            brandName = newItemBrand;
        }

        /// <summary>
        /// Constructor provided given the item name
        /// </summary>
        public BasketItem(string input)
        {
            this.itemDescription = input;
            this.basketItemID = -1;
            this.brandName = "UserEntered";
        }

        public override string ToString()
        {
            return brandName + " " + itemDescription;
        }

    }
}
