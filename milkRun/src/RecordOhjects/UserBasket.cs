﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace milkRun.recordOhjects
{
    public class UserBasket 
    {
        public int basketID { get; set; }
        public string basketName { get; set; }
        public List<BasketItem> basketItems { get; set; }       

        int createdByID;
        int lastModifedID;
        DateTime createdTS;
        DateTime lastModifiedTS;
        DateTime effStartTS;
        DateTime effStopTS;



        public UserBasket()
        {
            this.basketItems = new List<BasketItem>();
            this.basketName = "Name:" + RandomString(5);

            this.createdByID = (App.Current as App).appUser.userID;
            this.lastModifedID = (App.Current as App).appUser.userID;
            this.createdTS = DateTime.Now;
            this.lastModifiedTS = DateTime.Now;
        }

        public UserBasket(bool isTest)
        {
            this.basketID = -1;
            this.basketItems = new List<BasketItem>();
            this.basketName = "Name:" + RandomString(5);

            this.basketItems = new List<BasketItem>();
            Random random = new Random();

            switch (random.Next(2))
            {
                case 0:
                    basketItems.Add(new BasketItem("Rootbeer"));
                    basketItems.Add(new BasketItem("Soda Crackers"));
                    basketItems.Add(new BasketItem("Crest Toothpaste"));
                    break;

                case 1:
                    basketItems.Add(new BasketItem("Chocolate Chips"));
                    basketItems.Add(new BasketItem("Frozen Pizza"));
                    basketItems.Add(new BasketItem("Pickles"));
                    break;
                case 2:
                    basketItems.Add(new BasketItem("Tetley Tea"));
                    basketItems.Add(new BasketItem("Green Giant peas"));
                    basketItems.Add(new BasketItem("Dinner Rolls (x15)"));
                    break;
            }

            this.createdByID = (App.Current as App).appUser.userID;
            this.lastModifedID = (App.Current as App).appUser.userID;
            this.createdTS = DateTime.Now;
            this.lastModifiedTS = DateTime.Now;
        }

        public UserBasket(List<BasketItem> currentBasket, int newBasketID)
        {
            this.basketID = newBasketID;
            this.basketItems = currentBasket;
        }



        private readonly Random _rng = new Random();
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private string RandomString(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
            }
            return new string(buffer);
        }

    }
}
