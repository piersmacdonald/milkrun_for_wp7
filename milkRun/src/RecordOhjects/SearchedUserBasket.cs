﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace milkRun.recordOhjects
{
    public class SearchedUserBasket : UserBasket
    {
        public List<BasketSearchResults> searchResults { get; set; }

        int createdByID;
        int lastModifedID;
        DateTime createdTS;
        DateTime lastModifiedTS;
        DateTime effStartTS;
        DateTime effStopTS;


        /// <summary>
        /// Test constructor to make a dummy object
        /// </summary>
        public SearchedUserBasket(bool test)
        {
            searchResults = new List<BasketSearchResults>();

            searchResults.Add(new BasketSearchResults(true));
            searchResults.Add(new BasketSearchResults(true));
            searchResults.Add(new BasketSearchResults(true));

            this.createdByID = (App.Current as App).appUser.userID;
            this.lastModifedID = (App.Current as App).appUser.userID;
            this.createdTS = DateTime.Now;
            this.lastModifiedTS = DateTime.Now;
            this.effStartTS = DateTime.Now;
        }

        public SearchedUserBasket(UserBasket inputBasket)
        {

            searchResults = new List<BasketSearchResults>();


            searchResults.Add(new BasketSearchResults(inputBasket));
            searchResults.Add(new BasketSearchResults(inputBasket));
            searchResults.Add(new BasketSearchResults(inputBasket));

        }

    }
}
