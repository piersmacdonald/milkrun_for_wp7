﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;


namespace milkRun.recordOhjects
{
    public class applicationSettings
    {
        public string applicationName { get; set; }
        public AppUser user { get; set; }
        public Dictionary<string, string> phraseDictionary = new Dictionary<string, string>();

        public applicationSettings()
        {
            user = new AppUser(23, "Piers");

            phraseDictionary.Add("userName", "Welcome ");
            phraseDictionary.Add("welcomeText", "You're using Milk Run. This isn't only an electronic shopping list, it's a dynamic grocery pricing tool that will ensure you not only" +
                            "get everything you need but know where to get it and at the best price. The best way to learn more is to try it out yourself: create a new basket, or " +
                            "select an old one, and enter some items you may want.");
        }

        public string getPhrase(string langCode)
        {
            return phraseDictionary[langCode];

        }
    }
}
