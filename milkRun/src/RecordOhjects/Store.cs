﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Device.Location;
using Microsoft.Phone.Controls.Maps.Platform;

namespace milkRun.recordOhjects
{
    

    public class Store
    {
        public int storeID { get; set; }
        public string name { get; set; }
        public string logoImageLocation { get; set; }

        /*
         * Geolocation related vars 
         */
        public GeoCoordinate geoLocation { get; set; }
        public CivicAddress address { get; set; }
        public Location location {get; set;}

        //admin vars
        int createdByID { get; set; }
        int lastModifedID { get; set; }
        DateTime createdTS { get; set; }
        DateTime lastModifiedTS { get; set; }
        DateTime effStartTS { get; set; }
        DateTime effStopTS { get; set; }

        public Store()
        {
            string[] names = { "Safeway", "IGA", "Costco", "Whole Foods", "Nestors", "Thriftys" };
            name = names[new Random().Next(0, names.Length)];
            geoLocation = new GeoCoordinate(47.620574, -122.34942);

            storeID = 2;
            //location.Latitude = 47.620574;
            //location.Longitude = -122.34942;
            
        }

        public string addressToString()
        {
            return address.AddressLine1 + " " + address.AddressLine2;
        }
    }
}
