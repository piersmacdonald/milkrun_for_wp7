﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using milkRun.recordOhjects;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;

namespace milkRun.services
{
    public class ItemPredictionUtils
    {
        public static String  requestString = "http://milk-run.appspot.com/itemPredict/{0}";

        public static List<SearchedBasketItem> itemListFromServiceResults(Stream s)
        {
            List<SearchedBasketItem> listFromService = new List<SearchedBasketItem>();

            XElement resultXml = XElement.Load(s);

            IEnumerable<XElement> items = resultXml.Descendants("basketItem");

            foreach (XElement i in items)
            {
                SearchedBasketItem newItem = new SearchedBasketItem(i.Element("itemDescription").Value, i.Element("brandName").Value);
                listFromService.Add(newItem);
            }
            return listFromService;
        }

        public static bool isValidSearchString(string s)
        {
            switch (s)
            {
                case "":
                    return false;
                case "Enter item here...":
                    return false;
            }

            return true;
        }
    }
}
