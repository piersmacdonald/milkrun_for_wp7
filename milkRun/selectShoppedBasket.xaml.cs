﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using milkRun.recordOhjects;
using System.Collections.ObjectModel;

namespace milkRun
{
    public partial class selectShoppedBasket : PhoneApplicationPage
    {
        public ObservableCollection<UserBasket> shoppedBaskets { get; set; }



        public selectShoppedBasket()
        {
            InitializeComponent();

            shoppedBaskets = new ObservableCollection<UserBasket>();
            
            //for testing purposes
            shoppedBaskets.Add(new UserBasket());
            shoppedBaskets.Add(new UserBasket());
            DataContext = this;
            //basketPivot.DataContext = shoppedBaskets;
        }

        private void PivotControl_SelectionChanged() { }
    }
}