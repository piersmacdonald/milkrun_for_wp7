﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using milkRun.recordOhjects;
using System.Collections.ObjectModel;
using milkRun.services;
using System.IO.IsolatedStorage;

namespace milkRun
{
    public partial class editBasket : PhoneApplicationPage
    {

        public ObservableCollection<BasketItem> predictedItems = new ObservableCollection<BasketItem>();
        private UserBasket _currentBasket = new UserBasket();
        
        //public ObservableCollection<BasketItem> currentBasket = new ObservableCollection<BasketItem>();
        public ObservableCollection<BasketItem> currentBasket { get; set; }

        WebClient wc = new WebClient();
        private IsolatedStorageSettings userLocalSettings = IsolatedStorageSettings.ApplicationSettings;
       
        public editBasket()
        {
            InitializeComponent();

            predictItemList.DataContext = predictedItems;
            

            //wc.OpenReadCompleted += new OpenReadCompletedEventHandler(predictService_OpenReadCompleted);
           // wc.OpenReadAsync(new Uri(String.Format(ItemPredictionUtils.requestString, "br")));

        }

        private void addItem_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = sender as Button;
            BasketItem newItem = clickedButton.DataContext as BasketItem;
            currentBasket.Add(newItem);
        }

        private void addGenericItem_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                BasketItem newItem = new BasketItem(newItemEntry.Text);
                currentBasket.Add(newItem);
                newItemEntry.Text = "";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
            }
        }

        private void newItemEntry_GotFocus(object sender, RoutedEventArgs e)
        {
            if (newItemEntry.Text == "Enter item here...")
            {
                newItemEntry.Text = "";
            }
                    

        }

        //private void newItemEntry_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    if (ItemPredictionUtils.isValidSearchString(newItemEntry.Text) && wc.IsBusy)
        //    {
        //        // wc.CancelAsync();            
        //    }
        //    else if (ItemPredictionUtils.isValidSearchString(newItemEntry.Text) && !wc.IsBusy)
        //    {
        //        wc.OpenReadCompleted += new OpenReadCompletedEventHandler(predictService_OpenReadCompleted);
        //        wc.OpenReadAsync(new Uri(String.Format(ItemPredictionUtils.requestString, newItemEntry.Text)));
        //    }
        //}


        private void removeItem_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = sender as Button;
            BasketItem selectedItem = clickedButton.DataContext as BasketItem;
            currentBasket.Remove(selectedItem);

        }

        private void shopSelectedBasket_Click(object sender, RoutedEventArgs e)
        {
            if (currentBasket != null)
            {
                //convert the ObservableCollection to a List<> and send it to the appUser class to manage the user baskets
                _currentBasket.basketItems = CollectionToList();

                try
                {
                    _currentBasket.basketID = (App.Current as App).appUser.addUpdateUserBaskets(_currentBasket); 
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("shopSelectedBasket_Click failed running addUpdateUserBaskets");
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                }

                try
                {
                    NavigationService.Navigate(new Uri("/viewShoppedBaskets.xaml?basketID=" + _currentBasket.basketID, UriKind.RelativeOrAbsolute));
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("shopSelectedBasket_Click failed to naviage to viewShoppedBasket.xaml");
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace.ToString());
                }
                //navigate to the next page
               
            }
            else 
            {
                //if the current basket is empty there's no shopping going on.
                //TODO: generate a pop up or some kind of notification.
            }

            
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string input;
            if (NavigationContext.QueryString.TryGetValue("basketID", out input))
            {

                int bID = Int32.Parse(input);
                //get userBasket by ID
                try
                {
                    _currentBasket = (App.Current as App).appUser.getUserBasket(bID);
                    currentBasket = ListToCollection(_currentBasket);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    
                    currentBasket = new ObservableCollection<BasketItem>();
                    _currentBasket = new UserBasket();
                    //shit went down. add more robust exp handling later
                }

            }
            else
            {
                currentBasket = new ObservableCollection<BasketItem>();
                _currentBasket = new UserBasket();
            }

            currentBasketList.DataContext = currentBasket;
        }

        //void predictService_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        //{
            

        //    // You should always check to see if an error occurred. In this case, the application
        //    // simply returns.
        //    if (e.Error != null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Item Prediction Service return null");
        //        predictItemList.Visibility = Visibility.Collapsed;
        //        return;
        //    }
        //    else
        //    {
        //        try
        //        {

        //            predictedItems.Clear();
        //            List<SearchedBasketItem> itemsFromService = ItemPredictionUtils.itemListFromServiceResults(e.Result);

        //            foreach (SearchedBasketItem i in itemsFromService)
        //            {
        //                predictedItems.Add(i);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            System.Diagnostics.Debug.WriteLine(ex.Message);
        //        }
        //        finally
        //        {
        //            predictItemList.Visibility = Visibility.Visible;
        //        }
        //    }
        //}

        private void saveSelectedBasket_Click(object sender, RoutedEventArgs e)
        {
            if (currentBasket != null)
            {
                _currentBasket.basketItems = CollectionToList();
                _currentBasket.basketID = (App.Current as App).appUser.addUpdateUserBaskets(_currentBasket);

            }//if the current basket is empty there's nothing to save
            else
            {

            }
        }

        private ObservableCollection<BasketItem> ListToCollection(UserBasket basket)
        {
            ObservableCollection<BasketItem> newCollection = new ObservableCollection<BasketItem>();
            foreach (BasketItem i in basket.basketItems)
            {
                newCollection.Add(i);
            }

            return newCollection;
        }

        private List<BasketItem> CollectionToList()
        {
            List<BasketItem> tmpList = new List<BasketItem>();
            foreach (BasketItem i in currentBasket)
            {
                tmpList.Add(i);
            }

            return tmpList;
        }
    }
}